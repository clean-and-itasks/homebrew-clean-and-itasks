Installation instructions
=========================

```
brew tap clean-and-itasks/homebrew-clean-and-itasks https://gitlab.science.ru.nl/clean-and-itasks/homebrew-clean-and-itasks.git
brew install clean-and-itasks/clean-and-itasks/clean-itasks
```