class CleanItasks < Formula
  desc "Clean and iTasks"
  homepage "http://clean.cs.ru.nl/Clean"
  url "ftp://ftp.cs.ru.nl/pub/Clean/nightly/clean-itasks-osx-20160630.zip"
  version "20160630"
  sha256 "b61c7936eaafc47a689d9f6ab47bdd195de9211db89cda4acd98d4645bf7203d"

  def install
    system "make"
    prefix.install Dir["*"]
  end

  def caveats; <<-EOS.undent
    Add the following line to your ~/.bash_profile or ~/.zshrc file (and remember
    to source the file to update your current session):

      export CLEAN_HOME=#{prefix}
      export PATH=$CLEAN_HOME:$CLEAN_HOME/bin:$CLEAN_HOME/lib/exe:$PATH
    EOS
  end

  test do
    system "bin/cpm"
  end
end
